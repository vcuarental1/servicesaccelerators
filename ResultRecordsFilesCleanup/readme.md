******************* ResultsRecords&FilesRemoval *******************

/n Script that removes all orphan branches older than a specific number of days.

Parameter:
- removeRecordsOlderThanXDays: Minimum number of days old from the result records that will be deleted.
