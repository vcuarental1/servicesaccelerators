*************** Checkmarx integration with Certificate ***************. 

/n Checkmarx integration with a Certificate for a successful connection.  

This solution was provided to a customer which works with 3rd party CA certificates.  

/n NOTE: The certificate cannot be included in a system property due to a limitation. Even if the fields allows to introduce the certificate, In the execution we can find that it is trimmed to fit the allowed size.



