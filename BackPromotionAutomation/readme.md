*************** Back Promotion Automation ***************

> There is a script for each Pipeline Connection.
> Parameters:
    
- targetEnvironments: Back promotion target environments. This script will create a promotion record for each target environment.
- DestinationEnv: This is the source of the back promotinons for each connection.
- ReleaseId: Release to make sure that the scope of the back promotion is for one release and we do not hit the salesoforce limits.
- project: Name of the project.

 
