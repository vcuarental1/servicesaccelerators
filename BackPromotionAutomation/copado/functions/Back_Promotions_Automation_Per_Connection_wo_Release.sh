#!/bin/bash
set -eup pipefail

# Image name = copado-multicloud-metadata:v1
# Parameters = project_Name, parent_Env, child_EnvName, child_EnvName2, userstory_Status
# we should pass as a parameter the targetEnvironments, small changes in apex to parse a string parameter TARGET_ENVIRONMENTS="cpdtrn2htf cpdtrnsit" and split them in a list/set

cat <<__EOT__ > /tmp/run.apex
Set<String> targetEnvironments = new Set<String> {'${child_EnvName}','${child_EnvName2}'};
    Id DestinationEnv = [Select id from copado__Environment__c where Name='${parent_Env}' LIMIT 1].Id;
    List<copado__Org__c> list_orgcred = [SELECT Id, copado__Environment__c FROM copado__Org__c WHERE copado__Environment__r.Name IN :targetEnvironments AND copado__Default_Credential__c = true LIMIT 100];
    List<copado__User_Story__c> userStoryList = [select id, copado__status__c FROM copado__User_Story__c where copado__status__c='${userstory_Status}' ORDER BY CreatedDate DESC LIMIT 100];
    Id project = [SELECT Id FROM copado__Project__c WHERE Name = '${project_Name}' LIMIT 1].Id;
    
Set<String> uniqueKeyForCompletedPros = new Set<String>();
List<copado__Promoted_User_Story__c> completedUserStoryProm = [SELECT Id,copado__User_Story__c, copado__Promotion__r.copado__Source_Environment__c, copado__Promotion__r.copado__Destination_Environment__c, copado__Promotion__r.copado__Back_Promotion__c from copado__Promoted_User_Story__c where  copado__Status__c = 'Active' AND copado__Promotion__r.copado__Status__c ='Completed' AND ((copado__Promotion__r.copado__Back_Promotion__c=true AND copado__Promotion__r.copado__Source_Environment__c =: DestinationEnv ) OR (copado__Promotion__r.copado__Back_Promotion__c=false AND copado__Promotion__r.copado__Destination_Environment__c =: DestinationEnv )) LIMIT 200];
for(copado__Promoted_User_Story__c pus :completedUserStoryProm){
    uniqueKeyForCompletedPros.add(pus.copado__User_Story__c + String.valueOf(pus.copado__Promotion__r.copado__Back_Promotion__c) + (pus.copado__Promotion__r.copado__Back_Promotion__c ? pus.copado__Promotion__r.copado__Destination_Environment__c : pus.copado__Promotion__r.copado__Source_Environment__c));
    }


List<copado__Promotion__c> totalBackProm = new List<copado__Promotion__c>();

Map<id,copado__User_Story__c> mapUserStories = new Map<id,copado__User_Story__c> ();
mapUserStories.putall(userStoryList);

for (copado__Org__c orgCred : list_orgcred){
    copado__Promotion__c prom_i = new copado__Promotion__c(
        copado__Project__c = project,
        copado__Destination_Org_Credential__c = orgCred.Id,
        copado__Destination_Environment__c = orgCred.copado__Environment__c,
        copado__Back_Promotion__c = true
    );
    totalBackProm.add(prom_i);

}
insert totalBackProm;

List<copado__Promoted_User_Story__c> promotedStories = new List<copado__Promoted_User_Story__c>();
for (copado__Promotion__c promotion : totalBackProm) {
    for (copado__User_Story__c story : userStoryList) {
        if(!uniqueKeyForCompletedPros.contains(story.Id + String.valueof(false) + promotion.copado__Destination_Environment__c) && !uniqueKeyForCompletedPros.contains(story.Id + String.valueof(true) + promotion.copado__Destination_Environment__c)){
            copado__Promoted_User_Story__c promoteStory = new copado__Promoted_User_Story__c();
            promoteStory.copado__User_Story__c = story.Id;
            promoteStory.Name = 'Daily Back Promotion - ' + Date.today().format();
            promoteStory.copado__Promotion__c = promotion.Id;
            promotedStories.add(promoteStory);   
        }
    }
}

insert promotedStories;

for (copado__Promotion__c promotion : totalBackProm) {
    copado.RunDeployPromotion.InvocableVariables invocableVariable = new copado.RunDeployPromotion.InvocableVariables();
    invocableVariable.checkOnly = false;
    invocableVariable.deploymentName = 'Daily Back Promotion - ' + Date.today().format();
    invocableVariable.promotionId = promotion.Id;
    invocableVariable.testLevel = 'NoTestRun';
    List<copado.RunDeployPromotion.InvocableVariables> invocableVariables = new List<copado.RunDeployPromotion.InvocableVariables>{invocableVariable};
    copado.RunDeployPromotion.execute(invocableVariables);
}
__EOT__

# fix urls
export CF_SF_ENDPOINT="https://$(echo $CF_SF_ENDPOINT | sed -e 's/[^/]*\/\/\([^@]*@\)\?\([^:/]*\).*/\2/')"

SFDX_ACCESS_TOKEN="$CF_SF_SESSIONID" sf org login access-token --alias copadoOrg --instance-url "$CF_SF_ENDPOINT" --no-prompt
sf apex run --file /tmp/run.apex --target-org copadoOrg --json