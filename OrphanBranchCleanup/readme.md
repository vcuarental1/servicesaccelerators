******************* OrphanBranchesRemoval *******************

/n Script that removes all orphan branches older than a specific number of days.

Parameter:
- daysAgo: Minimum number of days old from the orphan branches that will be deleted.
